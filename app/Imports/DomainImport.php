<?php

namespace App\Imports;

use App\Domain;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class DomainImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        return new Domain([
            'tendomain' => $row['ten_domain'],
            'status' => $row['trang_thai'],
            'giatien' => $row['gia_tien'],

        ]);
    }
    
    public function headingRow(): int
    {
        return 5;
    }
}
